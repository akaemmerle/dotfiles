This project contain my personal dotfiles, based on the [Dotfiles Are Meant to Be Forked](https://zachholman.com/2010/08/dotfiles-are-meant-to-be-forked/) post by [Zach Holman](https://gitlab.com/holman).

## Installation

```sh
git clone git@gitlab.com:akaemmerle/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
script/bootstrap
```

This will symlink the appropriate files in `.dotfiles` to your home directory.  
Everything is configured and tweaked within `~/.dotfiles`.

## Contribution

Feel free to open an issue in this project and assign it to @akaemmerle.

## Thanks

Inspired by [Zach Holman](https://github.com/holman/dotfiles), integrates with my [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) configuration.
