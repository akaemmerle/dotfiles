# GDK
alias gdk.ce.cd="cd ~/git/gitlab/gdk-ce/gitlab/"
alias gdk.ce.run="cd ~/git/gitlab/gdk-ce/ && gdk run"

alias gdk.ee.cd="cd ~/git/gitlab/gdk-ee/gitlab/"
alias gdk.ee.run="cd ~/git/gitlab/gdk-ee/ && gdk run"

alias gdk.kill="pkill -f chromedriver chromium-browser ruby ruby2.3 node postgres redis redis-server"
alias gdk.killall="killall redis-server postgres ruby node gitaly chromedriver ruby gitlab-workhorse gitlab-runner gitlab-runner-bleeding unicorn_rails"

# www
alias gl.www.cd="cd ~/git/gitlab/www-gitlab-com/"

# Pajamas
alias gl.ds.cd="cd ~/git/gitlab/design.gitlab.com/"
alias gl.ds.start="cd ~/git/gitlab/design.gitlab.com/ && yarn start"

# gitlab-ui
alias gl.ui.cd="cd ~/git/gitlab/gitlab-ui/"
alias gl.ui.start="cd ~/git/gitlab/gitlab-ui/ && yarn storybook"
